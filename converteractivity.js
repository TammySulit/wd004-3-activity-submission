// CONVERTER Activity

// Values: 
const oneDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];

const tens = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];

const teens = ["", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"];

const hundreds = ["", "One Hundred", "Two Hundred", "Three Hundred", "Four Hundred", "Five Hundred", "Six Hundred", "Seven Hundred", "Eight Hundred", "Nine Hundred"];


// Function Section
function converter(number) {

	if( number <= 9 ){
		return oneDigit[number];
	} 

	if (number%10 === 0 && number < 100){
		const tensIndex = number/10;
		return tens[tensIndex];
	}

	if( number > 10 && number < 20) {
		const teensIndex = number - 10;
		return teens[teensIndex];
	}

	if( number < 100 ){
		const onesIndex = number % 10;
		const numberWithoutRemainder = number - onesIndex;
		const tensIndex = numberWithoutRemainder / 10;
		return tens[tensIndex] + " " +oneDigit[onesIndex];
	}
// ===================================================================

// ACTIVITY SECTION

	if (number < 1000) {
		const byTwo = number % 100;
		const byFirst = byTwo % 10;
		const byHundred = (number - byTwo) / 100;
		const byTens = (byTwo - byFirst) / 10;

	if (byTwo >= 11 && byTwo <= 19) {
		const tempTwo = byTwo - 10;
		return oneDigit[byHundred] + " hundred " + tens[tempTwo];
		}
	if (byFirst >= 1 && byFirst <= 9){
		if (byTens === 0) {
		return oneDigit[byHundred] + " hundred " + oneDigit[byFirst];
			}
		}
	if (byTens !== 0 && byFirst === 0) {
		return oneDigit[byHundred] + " hundred " + tens[byTens];
		}
	if (byTens === 0 && byFirst === 0) {
		return oneDigit[byHundred] + " hundred "
		}
		return oneDigit[byHundred] + " hundred " + tens[byTens] + " " + oneDigit[byFirst];
	}

	if (number < 10000) {

		const byThree = number % 1000;
		const byTwo = byThree % 100;
		const byFirst = byTwo % 10;
		const byThousand = (number - byThree) / 1000;
		const byHundred = (byThree - byTwo) / 100;
		const byTens = (byTwo - byFirst) / 10;

		if (byHundred === 0 && byTens !==0 && byFirst === 0) {
			return oneDigit[byThousand] + " thousand " + tens[byTens];
		}
		if (byHundred !== 0 && byTens !==0 && byFirst === 0) {
			return oneDigit[byThousand] + " thousand " +  oneDigit[byHundred] + " hundred " + tens[byTens];
		}
		if (byTwo >= 11 && byTwo <= 19) {
			const tempTwo = byTwo - 10;
			if (byHundred === 0) {
				return oneDigit[byThousand] + " thousand " +  teens[tempTwo];
			}	
		}
		if (byFirst >= 1 && byFirst <= 9){
			if (byHundred === 0 && byTens !== 0) {
				return oneDigit[byThousand] + " thousand " + tens[byTens] + oneDigit[byFirst];
			}else if (byHundred !== 0 && byTens === 0) {
				return oneDigit[byThousand] + " thousand " +  oneDigit[byHundred] + " hundred " + oneDigit[byFirst];
			}else if (byHundred === 0 && byTens === 0){
				return oneDigit[byThousand] + " thousand " + oneDigit[byFirst];
			}
		}
		if (byHundred !== 0 && byTens === 0 && byFirst === 0) {
			return oneDigit[byThousand] + " thousand " +  oneDigit[byHundred] + " hundred ";
		}
		if (byHundred === 0 && byTens === 0 && byFirst === 0) {
			return oneDigit[byThousand] + " thousand ";
		}

		return oneDigit[byThousand] + " thousand " +  oneDigit[byHundred] + " hundred " + tens[byTens] +" "+ oneDigit[byFirst];
	}
	return "Invalid Data";
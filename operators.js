// Create a new file: operators.js

// 1. Arithmetic
// 2. Assignment
// 3. Arithmtic - Assignment
// 4. Comparison Operators
		A. Equality Operator (==)
		It compars the values regardless of data type.

		2 == 2 // true
		2 == 3 // false
		2 == '2' // true
		0 == false // true --truthy
		1 == false // false -- falsy
		1 == true // true -- truthy
		the rest == false // falsy 

		B. Strict Equality Operator (===)
        It compares the vales and the data types.
        checks value and data type

        2 === '2' //false
        1 === true //false

        C. Inequality Operator (!=)
        if the values are not equal, it will return true; 
        (REGARDLESS OF DATA TYPE)

        D. Strict Inequality Operator (!==)
        if the values OR the data types are not equal, it will return true.

NOTE: In Javascript (! -> NOT)
> greater than
< less than
>= greater than or equal to 
<= less than or equal to

Academind Javascript

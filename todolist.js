// // Create a to do list with the ff functions

// 1. showToDos -- will return an array of all to showToDos

// a. Create an empty todos array
// Ex. const todos = [];

// b. Create a function that will add a new todo. 
// Things to consider:
//    - What is the data type of the todo data? String? Object?
//    - What are the parameteres that this function needs?
//    - Push the newTodo into our todos array.
const todos = [];

function addTodo(task){
	const newTodo = task;
	todos.push(newTodo);
	return "Successfully added " + newTodo;
}

// 2. addToDos (todo) -- will add a new todo in our array of addToDos

function showTodo(){
	return todos;
}

function updateTodo(index,updatedTask){
// 3. updateToDo() -- Given an index will update the todo
	// Things to consider:
	// 	-When updating a data, we need something to identify which data we want to update.
	// 	In this case, we will use the index.
	// 	- We need the updated value.
		todos[index]= updatedTask;
		return "Successfully updated task!";
}

// function addToDo(){

// }

function deleteTodo(index){
// 4. deleteToDo() -- Given and index will delete a todo
	// Things to consider:
	//    - When deleting a data, we need something to identify whuch data we want to delete.
	//    todos.splice(index,1)

	todos.splice(index,1);
	return "Successfully deleted task!";
}
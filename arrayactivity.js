//arrayActivity.js

// Task: Create a Pila Management System with the following functions.
// 
const pila = [];

// 1. showAll
//  will return an array of those who are in line.

function showAll(){
  return pila;
}
//  
//  2. addQueue()
//  accepts a name as a parameter(string), will add the data at the end of the array.

function addQueue(string){
  newQueue = string;
  const addQueue = pila.push(newQueue);
  return addQueue;
}

//  
//  3. deQueue()
//  NO PARAMETER. removes the first person in the array.
function deQueue(){
    const deQueue = pila.shift();
    return deQueue;
  }
 
//  4. expedite(index)
//  It will remove the data based on the given index

function expedite(index){
  const expedite = pila.splice(index, 1)
  return expedite;
}

//  5. reverseDequeue()
//  NO PARAMETER. removes the last person in the array

function reverseDequeue(){
    const reverseDequeue = pila.pop();
    return reverseDequeue;
}

//  
//  6. addVIP()
//  accepts a name as a parameter(string),
//  add the data at the beginning of the array

function addVIP(string){
  const addVIP = pila.unshift(string);
  return addVIP;
}

//  
//  7. updateName(index, newName)
//  update the data based on the given parameters.

function updateName(index, newName){
    const nameIndex = pila[index];
    const updateName = 
    return updateName;

}

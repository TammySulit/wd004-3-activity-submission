// Condition --- these are statements that will either result to true or falls;

// 1. if
// if (condition){
	//task you want to execute.
// }

// it will only execut the task if the condition is true.

function checkAge(age){
	if(age>18){
		return "Legal ka na."
	};
};

// 2. if-else
if(condition){
	task you want to execute if the condition is true } else {
		task you want to execute if the condition = false; 
	}

function checkAge(age){
	if(age>=18){
		return "Legal ka na."
	}else{
		return "Minor ka pa";
	}
}

// This function checks the age and return a string value @param {number} age User-input age

function whereYouShouldBe(age){
	//0-5 with Mommy/Daddy
	//6-12 Elementary Schoold
	//13-18 High School
	//22-23 Soul Searching
	//24-50 Work
	//51- Enoying Life

	if(age<=5){
		return "With Mommy/Daddy";
	}else{
		return "Elementary School";
	}
	

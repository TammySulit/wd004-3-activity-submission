// console.log(hello);
// Array -- is a collection of related data. Denoted by []

// Ex. 
const fruits = ['Apple', 'Banana', 'Kiwi'];

// const ages = [1,2,3,4];

// const students = [ 
// 	{name: "Brandon", age: 11}, 
// 	{name: "Brenda", age: 12},
// 	{name: "Celmer", age: 15},
// 	{name: "Archie", age: 16}

// 	];

// ACCESS: 
// students[1];
// students[3].name;
// students[3].age;


// 1.It is recommended to use plural form when naming an array, and singular when naming an object
// can be accessed via bracket notation

// 2. //to access an array, we need to use bracket notation and the index of the item we want to access.

// INDEX - is the position of the data relative to the position of the first data
// - always starts at 0

// 3. // To add a data in an array, we will use the method .push();
// array.push(dataWeWantToAdd); the method will return the lenght of the array.
// length means # of data inside an array
//The new data will be added at the end

// PUSH - ADD DATA: 
// fruits; 
// (4) ["Apple", "Banana", "Kiwi", "Blueberry"]

// Ex. ages.push({name:"dy", age:18})


// 4. To update a data, access the data first via bracket notation and then re-assign a value.
// 5. To deleta a data at the end of an array, we use the method pop();
// - it will return the deleted data;

// Ex.
// fruits.pop();
// "Blueberry"

//6. To delete a data at the start of an array, we use the method shift();
// array.shift();
// It will return the deleted data;

// 7. To add a data in the beginning of an array, we use the method unshift(dataWeWantToAdd);
// array.unshift(dataWeWantToAdd);
// This will return the new length of the array

// Push--- add end
// Pop --- delete end
// Shift --- delete start
// Unshift --- add start

// 8. .splice();
// It takes at least 2 arguments.
// .splice( startingIndex,noOfItemsYouWantToDelete );
// This will return an array of deleted items; 

// Ex. fruits.splice(1,2);
    // fruits.splice(1,0,"Orange"); -- insert orange on 1 index
    // fruits.splice(1,1,"Orange","Duhat", "Bayabas"); -- insert 3 arguments from starting index


